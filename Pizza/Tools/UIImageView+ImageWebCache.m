//
//  UIImageView+ImageWebCache.m
//  RentApp
//
//  Created by iSashok on 9/29/14.
//  Copyright (c) 2014 Alexander Cherushnikov. All rights reserved.
//

#import "UIImageView+ImageWebCache.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation UIImageView (ImageWebCache)

- (void)webCache_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage {
    @weakify(self);
    [self sd_setImageWithURL:url placeholderImage:placeholderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        @strongify(self);
        [UIView transitionWithView:self
                          duration:0.2
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.image = image;
                        }
                        completion:nil];
    }];
}


@end
