//
//  UIImageView+ImageWebCache.h
//  RentApp
//
//  Created by iSashok on 9/29/14.
//  Copyright (c) 2014 Alexander Cherushnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (ImageWebCache)
- (void)webCache_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage;
@end
