//
//  AppDelegate.h
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

