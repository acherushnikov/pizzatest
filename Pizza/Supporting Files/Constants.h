//
//  Constants.h
//  OzonOnlineStore
//
//  Created by Андрей Останин on 24.05.12.
//  Copyright (c) 2012 OZON. All rights reserved.
//

#ifndef OzonOnlineStore_Constants_h
#define OzonOnlineStore_Constants_h

extern NSString * const EMPTY_STRING;

extern NSString * const AppFont;
extern NSString * const AppFontMedium;
extern NSString * const AppFontRegular;

#endif
