//
//  Constants.m
//  OzonOnlineStore
//
//  Created by Андрей Останин on 22.06.12.
//  Copyright (c) 2012 OZON. All rights reserved.
//

#import "Constants.h"
NSString * const EMPTY_STRING = @"";

NSString * const AppFont = @"HelveticaNeue-Light";
NSString * const AppFontMedium = @"HelveticaNeue-Medium";
NSString * const AppFontRegular = @"HelveticaNeue";
