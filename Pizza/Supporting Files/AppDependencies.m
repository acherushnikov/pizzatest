//
//  AppDependencies.m
//  MetroAlarm
//
//  Created by Александр Черушников on 14.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "AppDependencies.h"

#import "RootWireframe.h"
#import "RestaurantWireframe.h"
#import "RestaurantPresenter.h"
#import "RestaurantInteractor.h"

@interface AppDependencies ()
@property(nonatomic,strong)RestaurantWireframe *restaurantWireframe;
@end

@implementation AppDependencies

- (id)init {
    if ((self = [super init])) {
        [self configureDependencies];
    }
    return self;
}


- (void)installRootViewControllerIntoWindow:(UIWindow *)window {
    [self.restaurantWireframe presentRestaurantInterfaceFromWindow:window];
}

- (void)configureDependencies {
    // Root Level Classes

    RootWireframe *rootWireframe = [[RootWireframe alloc] init];
    
    // Restourant Modules Classes
    RestaurantWireframe *restaurantWireframe = [[RestaurantWireframe alloc] init];
    RestaurantPresenter *restaurantPresenter = [[RestaurantPresenter alloc] init];
    RestaurantInteractor *restaurantInterator = [[RestaurantInteractor alloc] init];

    // List Module Classes
    restaurantInterator.output = restaurantPresenter;
    
    restaurantPresenter.restaurantInteractor = restaurantInterator;
    restaurantPresenter.restaurantWireframe = restaurantWireframe;
    
    restaurantWireframe.rootWireframe = rootWireframe;
    restaurantWireframe.restaurantPresenter = restaurantPresenter;
    self.restaurantWireframe = restaurantWireframe;
    
}

@end

