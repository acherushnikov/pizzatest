//
//  AppDependencies.h
//  MetroAlarm
//
//  Created by Александр Черушников on 14.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//


@interface AppDependencies : NSObject
- (void)installRootViewControllerIntoWindow:(UIWindow *)window;
@end
