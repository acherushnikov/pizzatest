//
//  RestourantModel.m
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestourantModel.h"
#import "NetworkManager.h"

NSString * const finishedDataLoadingRestourantEvent = @"ru.ozon.events.data_loading_restourant.finished";
NSString * const errorDataLoadingRestourantEvent = @"ru.ozon.events.data_loading_restouranterror_happened";

static NSString *const ErrorMessage = @"No result";

static NSString *const kNamePropertyKey = @"name";
static NSString *const kPhoneKey = @"phone";
static NSString *const kFormattedPhonePropertyKey = @"formattedPhone";
static NSString *const kAddressPropertyKey = @"address";
static NSString *const kCrossStreetPropertyKey = @"crossStreet";
static NSString *const kCityPropertyKey = @"city";
static NSString *const kPostalCodePropertyKey = @"postalCode";
static NSString *const kCountryPropertyKey = @"country";
static NSString *const kLatPropertyKey = @"lat";
static NSString *const kLngPropertyKey = @"lng";
static NSString *const kDistancePropertyKey = @"distance";
//static NSString *const kIconURLPrefixPropertyKey = @"iconURLPrefix";
//static NSString *const kIconURLSuffixPropertyKey = @"iconURLSuffix";
static NSString *const kResourantURLPropertyKey = @"resourantURL";
static NSString *const kResourantMenuURLPropertyKey = @"resourantMenuURL";

@implementation JSONRestourantItem
+(JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       kNamePropertyKey: kNamePropertyKey,
                                                       @"contact.phone": kPhoneKey,
                                                       @"contact.formattedPhone": kFormattedPhonePropertyKey,
                                                       @"location.address": kAddressPropertyKey,
                                                       @"location.crossStreet": kCrossStreetPropertyKey,
                                                       @"location.city": kCityPropertyKey,
                                                       @"location.postalCode": kPostalCodePropertyKey,
                                                       @"location.country": kCountryPropertyKey,
                                                       @"location.lat": kLatPropertyKey,
                                                       @"location.lng": kLngPropertyKey,
                                                       @"location.distance": kDistancePropertyKey,
//                                                       @"categories.icon.prefix": kIconURLPrefixPropertyKey,
//                                                       @"categories.icon.suffix": kIconURLSuffixPropertyKey,
                                                       @"url": kResourantURLPropertyKey,
                                                       @"menu.mobileUrl": kResourantMenuURLPropertyKey,
                                                       }];
}
@end

@interface RestourantModel ()
@property(nonatomic,strong)id request;
@property(nonatomic,strong)NSArray *restourantList;
@end

@implementation RestourantModel

- (void)outdate{
    if (self.request) {
        [[NetworkManager sharedManager] cancelOperation:self.request];
        self.request = nil;
    }
    self.restourantList = nil;
}

-(void)loadResourantsByLocation:(NSString *)location {
    @weakify(self);
    self.request = [[NetworkManager sharedManager] getRestourantsByLocation:location success:^(id jsonObject) {
       @strongify(self);
        self.request = nil;
        self.restourantList = [self mappedJSONAndReturnArray:jsonObject];
        XZAssertOrReturnBlock(self.restourantList != nil && self.restourantList.count != 0, (ErrorMessage), ^(NSError *err){
            [self trigger:errorDataLoadingRestourantEvent withContext:err.localizedDescription];

        });
        [self trigger:finishedDataLoadingRestourantEvent];
    } failure:^(NSInteger statusCode, NSString *errorString) {
        @strongify(self);
        [self trigger:errorDataLoadingRestourantEvent withContext:errorString];
    }];
}

-(NSArray *)mappedJSONAndReturnArray:(id)json {
    NSDictionary *response = json[@"response"];
    XZAssertOrReturnNil(response != nil);
    NSArray *venues = response[@"venues"];
    XZAssertOrReturnNil(venues != nil);
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:venues.count];
    NSError *err = nil;
    for (NSDictionary *jsobObj in venues) {
        JSONRestourantItem *restourantItem = [[JSONRestourantItem alloc] initWithDictionary:jsobObj error:&err];
        XZAssertOrReturnNil(restourantItem != nil);
        [arr addObject:restourantItem];
    }
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"name" ascending: YES];
    NSArray *sortedArr = [[arr copy] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sorter]];
    return sortedArr;
}

#pragma mark - Resources management
- (void)dealloc{
    if (_request) {
        [[NetworkManager sharedManager] cancelOperation:_request];
        _request = nil;
    }
}
@end
