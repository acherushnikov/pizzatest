//
//  RestourantModel.h
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import "JSONModel.h"

extern NSString * const finishedDataLoadingRestourantEvent;
extern NSString * const errorDataLoadingRestourantEvent;

@interface JSONRestourantItem : JSONModel
@property(nonatomic,strong) NSString<Optional>* name;
@property(nonatomic,assign) NSNumber<Optional>*phone;
@property(nonatomic,strong) NSString<Optional>* formattedPhone;
@property(nonatomic,strong) NSString<Optional>* address;
@property(nonatomic,strong) NSString<Optional>* crossStreet;
@property(nonatomic,strong) NSString<Optional>* city;
@property(nonatomic,strong) NSString<Optional>* postalCode;
@property(nonatomic,strong) NSString<Optional>* country;
@property(nonatomic,assign) NSNumber<Optional>*lat;
@property(nonatomic,assign) NSNumber<Optional>*lng;
@property(nonatomic,assign) NSNumber<Optional>*distance;
//@property(nonatomic,strong) NSString<Optional>* iconURLPrefix;
//@property(nonatomic,strong) NSString<Optional>* iconURLSuffix;
@property(nonatomic,strong) NSURL<Optional>* resourantURL;
@property(nonatomic,strong) NSURL<Optional>* resourantMenuURL;
@end

@interface RestourantModel : NSObject
@property(nonatomic,strong,readonly)NSArray *restourantList;
-(void)loadResourantsByLocation:(NSString *)location;
@end
