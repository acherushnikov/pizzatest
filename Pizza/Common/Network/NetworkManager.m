//
//  NetworkManager.m
//  RentApp
//
//  Created by iSashok on 9/23/14.
//  Copyright (c) 2014 Alexander Cherushnikov. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworkActivityLogger.h"
#import "Constants.h"

static NSString * const StandardErrorMessage = @"Some error happend";

//TODO: Don't use it in production is not safety :)
static NSString * const ClientId = @"NFP5N12U1BVKNARYKXTQ4BH0USSLS31W4YYARVE3U4SK1W3I";
static NSString * const ClientSecret = @"TJMFRIF3YQDNXRW2WIIUZNNRTYXIEHMO4FN5PQNDZVZSPBPA";

static NSString * const BaseUrl = @"https://api.foursquare.com/v2/";

@interface NetworkManager()
@property(nonatomic,strong)AFHTTPSessionManager *forsquerManager;

@end

@implementation NetworkManager

+ (instancetype)sharedManager {
    static NetworkManager *_sharedManager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
#ifdef DEBUG
        AFNetworkActivityLogger *networkLogger = [AFNetworkActivityLogger sharedLogger];
        networkLogger.level = AFLoggerLevelDebug;
        [networkLogger startLogging];
#endif
        _sharedManager = [[NetworkManager alloc] init];
        
    });
    
    return _sharedManager;
}

- (instancetype)init { 
    XZAssertOrReturnNil(self = [super init]);
    AFHTTPSessionManager *forsquerManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BaseUrl]];
    forsquerManager.requestSerializer = [AFJSONRequestSerializer serializer];
    forsquerManager.responseSerializer = [AFJSONResponseSerializer serializer];
    self.forsquerManager = forsquerManager;
    return self;
}
#pragma mark - Default Handlers

- (void)handleFailure:(void (^)(NSInteger statusCode, NSString *errorString))failure
            operation:(NSURLSessionDataTask *)operation
             sysError:(NSError *)sysError
           statusCode:(NSInteger)statusCode{
    if (statusCode) {
        [[NSURLCache sharedURLCache] removeCachedResponseForRequest:operation.currentRequest];
    }

    if (failure){
        NSString *errorString = nil;
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
        if (response){
            
            errorString = [[response allHeaderFields] objectForKey:@"Status"];
        }
        else{
            errorString = StandardErrorMessage;
            
        }
        
        failure(statusCode, errorString);
    }
}


- (NSURLSessionDataTask *)getRestourantsByLocation:(NSString *)location
                                           success:(void (^)(id jsonObject))success
                                           failure:(NetworkManagerFailureHandler)failure {
    
    NSURLSessionDataTask *op = [self.forsquerManager GET:@"venues/search"
                                        parameters:@{
                                                     @"client_id": ClientId,
                                                     @"client_secret": ClientSecret,
                                                     @"v": @(20130815),
                                                     @"ll": location,
                                                     @"query": @"pizza"
                                                     }
                                           success:^(NSURLSessionDataTask *operation, id json) {
                                               if (success)
                                                   success(json);
                                           }
                                           failure:^(NSURLSessionDataTask *operation, NSError *error) {
                                               NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
                                               [self handleFailure:failure operation:operation sysError:error statusCode:response.statusCode];
                                           }];
    
    return op;
}

- (void)cancelOperation:(id)operationId {
    XZAssert([operationId isKindOfClass:[NSURLSessionDataTask class]], @"operationId is %@ should be %@.\n%@", NSStringFromClass([operationId class]), [NSURLSessionDataTask class], operationId);
    NSURLSessionDataTask *operation = operationId;
    [operation cancel];
}

@end
