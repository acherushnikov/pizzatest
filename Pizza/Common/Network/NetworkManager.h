//
//  NetworkManager.h
//  RentApp
//
//  Created by iSashok on 9/23/14.
//  Copyright (c) 2014 Alexander Cherushnikov. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void (^NetworkManagerFailureHandler)(NSInteger statusCode, NSString *errorString);

@interface NetworkManager : NSObject

#pragma mark - init
+ (instancetype)sharedManager;

/**
 *	Cancel operation by Id
 *
 *	@param operationId uniq id of task
 */
- (void)cancelOperation:(id)operationId;

/**
 *  Get restourant list by location
 *
 *  @param location by format "lat,long"
 *  @param success succes callback with JSON obj
 *  @param failure callback
 *
 */
- (NSURLSessionDataTask *)getRestourantsByLocation:(NSString *)location
                                         success:(void (^)(id jsonObject))success
                                         failure:(NetworkManagerFailureHandler)failure;

@end
