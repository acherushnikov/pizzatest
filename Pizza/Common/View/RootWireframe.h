//
//  RootWireframe.h
//  MetroAlarm
//
//  Created by Александр Черушников on 14.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RootWireframe : NSObject
- (void)showRootViewController:(UIViewController *)viewController
                      inWindow:(UIWindow *)window;
@end
