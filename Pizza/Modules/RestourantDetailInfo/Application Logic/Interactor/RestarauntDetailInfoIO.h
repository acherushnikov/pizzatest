//
//  RestarauntDetailInfoIO.h
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestarauntDetailInfoInteractorInput <NSObject>

@end

@protocol RestarauntDetailInfoInteractorOutput <NSObject>

@end