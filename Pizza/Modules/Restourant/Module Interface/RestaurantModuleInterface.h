//
//  HomeModuleInterface.h
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestaurantModuleInterface <NSObject>
- (void)startUpdateUserLocation;
@end
