//
//  HomeInteractor.m
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantInteractor.h"
#import "RestourantModel.h"

@interface RestaurantInteractor ()
@property (strong,nonatomic) RestourantModel *model;
@end

@implementation RestaurantInteractor
- (instancetype)init {
    XZAssertOrReturnNil(self = [super init]);
    _model = [[RestourantModel alloc] init];
    @weakify(self);
    [_model on:finishedDataLoadingRestourantEvent do:^{
        @strongify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.output loadedRestaurantList:self.model.restourantList];
        });
    }];
    [_model on:errorDataLoadingRestourantEvent doWithContext:^(NSString *errorString) {
        
    }];
    return self;
}

#pragma mark - RestaurantInteractorInput
-(void)getRestaraiuntByLocation:(NSString *)location {
    [self.model loadResourantsByLocation:location];
}
@end
