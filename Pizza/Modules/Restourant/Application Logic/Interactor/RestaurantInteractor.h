//
//  HomeInteractor.h
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantInteractorIO.h"

@interface RestaurantInteractor : NSObject <RestaurantInteractorInput>
@property(nonatomic,weak)id<RestaurantInteractorOutput> output;
@end
