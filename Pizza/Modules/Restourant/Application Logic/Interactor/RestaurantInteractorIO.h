//
//  HomeInteractorIO.h
//  MetroAlarm
//
//  Created by Александр Черушников on 22.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestaurantInteractorInput <NSObject>
-(void)getRestaraiuntByLocation:(NSString *)location;
@end

@protocol RestaurantInteractorOutput <NSObject>
-(void)loadedRestaurantList:(NSArray *)list;
@end