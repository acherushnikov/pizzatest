//
//  ViewController.m
//  EnglandMetro
//
//  Created by iSashok on 14.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantViewController.h"
#import "RestaurantCell.h"
#import "RestourantModel.h"

#pragma mark - Unit constant
static NSString *const RestaurantCellIdentifier = @"RestaurantCell";
static NSString *const Title = @"Restaurant";
static CGFloat const DefaultCellHeight = 78;

@interface RestaurantViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *restaurantList;
@end

@implementation RestaurantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([RestaurantCell class]) bundle:nil] forCellReuseIdentifier:RestaurantCellIdentifier];
    self.title = Title;
    [self.eventHandler startUpdateUserLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.restaurantList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    RestaurantCell * cell = [tableView dequeueReusableCellWithIdentifier:RestaurantCellIdentifier];
    JSONRestourantItem *item = self.restaurantList[indexPath.row];
    [cell setupFromItem:item];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return DefaultCellHeight;
}

#pragma mark - RestaurantViewInterface
-(void)updateViewWithList:(NSArray *)list {
    self.restaurantList = list;
    [self.tableView reloadData];
}
@end
