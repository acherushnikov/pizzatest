//
//  RestaurantCell.m
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantCell.h"
#import "UIImageView+ImageWebCache.h"
#import "RestourantModel.h"

@interface RestaurantCell ()
@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@end

@implementation RestaurantCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCoverImageFromUrl:(NSURL *)url {
    [self.coverImageView webCache_setImageWithURL:url placeholderImage:nil];
}

- (void)setFormattedPhone:(NSString *)phone {
    self.phoneLabel.text = phone;
}

-(void)setFormattedAddress:(NSString *)address {
    self.addressLabel.text = address;
}

- (void)setName:(NSString *)name {
    self.nameLabel.text = name;
}

@end

@implementation RestaurantCell (SetupRestaurantCell)
-(void)setupFromItem:(JSONRestourantItem *)item {
    [self setName:item.name];
    [self setFormattedAddress:[NSString stringWithFormat:@"%@, %@, %@, %@, %@", item.address,item.crossStreet,item.city, item.postalCode, item.country]];
    [self setFormattedPhone:item.formattedPhone];
//    [self setCoverImageFromUrl:[NSURL URLWithString:[NSString stringWithFormat:@"%@.%@",item.iconURLPrefix,item.iconURLSuffix]]];
}
@end