//
//  HomeViewInterface.h
//  MetroAlarm
//
//  Created by Александр Черушников on 21.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestaurantViewInterface <NSObject>
-(void)updateViewWithList:(NSArray *)list;
@end
