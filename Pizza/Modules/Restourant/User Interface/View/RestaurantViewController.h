//
//  ViewController.h
//  EnglandMetro
//
//  Created by iSashok on 14.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantModuleInterface.h"
#import "RestaurantViewInterface.h"

@interface RestaurantViewController : UIViewController <RestaurantViewInterface>
@property(nonatomic,strong)id<RestaurantModuleInterface>eventHandler;
@end

