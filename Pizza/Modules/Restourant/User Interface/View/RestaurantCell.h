//
//  RestaurantCell.h
//  Pizza
//
//  Created by iSashok on 20.10.15.
//  Copyright © 2015 Alexander Cherushnikov. All rights reserved.
//

@class JSONRestourantItem;

@interface RestaurantCell : UITableViewCell

@end

@interface RestaurantCell (SetupRestaurantCell)
-(void)setupFromItem:(JSONRestourantItem *)item;
@end
