//
//  HomePresenter.m
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantPresenter.h"
#import "RestaurantInteractor.h"
#import "RestaurantWireframe.h"

#import <CoreLocation/CoreLocation.h>

@interface RestaurantPresenter () <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation RestaurantPresenter

#pragma mark - RestaurantModuleInterface
- (void)startUpdateUserLocation {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

#pragma mark - RestaurantInteractorOutput
-(void)loadedRestaurantList:(NSArray *)list {
    [self.userInterface updateViewWithList:list];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {

    CLLocation *currentLocation = [manager location];
    NSString *location = [NSString stringWithFormat:@"%@,%@",@(currentLocation.coordinate.latitude),@(currentLocation.coordinate.longitude)];
    [manager stopUpdatingLocation];
    [self.restaurantInteractor getRestaraiuntByLocation:location];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    
}
@end
