//
//  HomePresenter.h
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantModuleInterface.h"
#import "RestaurantViewInterface.h"
#import "RestaurantInteractor.h"
#import "RestaurantInteractorIO.h"

#import "RestaurantInteractorIO.h"
@class RestaurantWireframe;

@interface RestaurantPresenter : NSObject <RestaurantModuleInterface, RestaurantInteractorOutput>
@property(nonatomic,strong) id<RestaurantInteractorInput>  restaurantInteractor;
@property(nonatomic,strong) RestaurantWireframe * restaurantWireframe;
@property(nonatomic,strong) UIViewController<RestaurantViewInterface> *userInterface;
@end
