//
//  HomeWireframe.h
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

@class RestaurantPresenter;
@class RestaurantViewController;
@class RootWireframe;

@interface RestaurantWireframe : NSObject
@property(nonatomic,strong)RestaurantPresenter *restaurantPresenter;
@property(nonatomic,strong)RootWireframe *rootWireframe;

- (void)presentRestaurantInterfaceFromWindow:(UIWindow *)window;
@end
