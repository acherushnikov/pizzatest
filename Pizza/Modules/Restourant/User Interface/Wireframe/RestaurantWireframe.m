//
//  restourantWireframe.m
//  MetroAlarm
//
//  Created by Александр Черушников on 15.09.15.
//  Copyright (c) 2015 Alexander Cherushnikov. All rights reserved.
//

#import "RestaurantWireframe.h"
#import "RestaurantPresenter.h"
#import "RestaurantViewController.h"
#import "RootWireframe.h"

static NSString *RestaurantControllerIdentifier = @"RestaurantViewController";

@interface RestaurantWireframe ()
@property(nonatomic,strong)RestaurantViewController *restaurantViewController;
@end

@implementation RestaurantWireframe

- (void)presentRestaurantInterfaceFromWindow:(UIWindow *)window {
    RestaurantViewController *restaurantViewController = [self restaurantViewControllerFromStoryboard];
    restaurantViewController.eventHandler = self.restaurantPresenter;
    self.restaurantPresenter.userInterface = restaurantViewController;
    self.restaurantViewController = restaurantViewController;
    
    [self.rootWireframe showRootViewController:restaurantViewController
                                      inWindow:window];
}

- (RestaurantViewController *)restaurantViewControllerFromStoryboard {
    UIStoryboard *storyboard = [self mainStoryboard];
    RestaurantViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:RestaurantControllerIdentifier];
    
    return viewController;
}


- (UIStoryboard *)mainStoryboard{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:[NSBundle mainBundle]];
    return storyboard;
}
@end
